package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Agenda implements IRepository{

    private String host = "localhost";
    private int port = 9999;
    private static int identifierCount = 1;
    private int idenfitier = 0;
    private int iid = 1;

    public Agenda() {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 1;
        boolean ok = false;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to create agenda: 1
            dos.writeInt(codOp);
            dos.writeInt(identifierCount);
            this.idenfitier = identifierCount;
            identifierCount++;
            ok = dis.readBoolean();
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void write(String s, int v) {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 2;
        boolean ok = false;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to write into agenda:
            dos.writeInt(codOp);
            dos.writeInt(this.idenfitier);
            dos.writeUTF(s);
            dos.writeInt(v);
            ok = dis.readBoolean();
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public int read(String s) {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 3;
        int number = 0;
        boolean ok = false;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to write into agenda:
            dos.writeInt(codOp);
            dos.writeInt(this.idenfitier);
            dos.writeUTF(s);
            ok = dis.readBoolean();
            if (ok) {
                number = dis.readInt();
            }
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return number;
    }
}
