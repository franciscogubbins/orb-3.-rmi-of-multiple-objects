package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Time implements ITime{

    private int iid = 2;
    private String host = "localhost";
    private int port = 9999;
    private static int identifierCount = 0;
    private int identifier = 0;

    public Time() {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 1;
        boolean ok = false;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to create agenda: 1
            dos.writeInt(codOp);
            dos.writeInt(identifierCount);
            this.identifier = identifierCount;
            identifierCount++;
            ok = dis.readBoolean();
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int getHour() {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 2;
        boolean ok = false;
        int hour = 0;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to write into agenda:
            dos.writeInt(codOp);
            dos.writeInt(this.identifier);
            ok = dis.readBoolean();
            if (ok) {
                hour = dis.readInt();
            }
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hour;
    }

    @Override
    public int getMinute() {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 3;
        boolean ok = false;
        int minute = 0;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to write into agenda:
            dos.writeInt(codOp);
            dos.writeInt(this.identifier);
            ok = dis.readBoolean();
            if (ok) {
                minute = dis.readInt();
            }
            return minute;
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return minute;
    }

    @Override
    public int getSeconds() {
        Socket cs = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        int codOp = 4;
        boolean ok = false;
        int seconds = 0;
        try {
            cs = new Socket(host, port);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());
            // send iid
            dos.writeInt(iid);
            // Code operation to write into agenda:
            dos.writeInt(codOp);
            dos.writeInt(this.identifier);
            ok = dis.readBoolean();
            if (ok) {
                seconds = dis.readInt();
            }
            return seconds;
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return seconds;
    }
}
