package client;

public interface ITime {
    public int getHour();
    public int getMinute();
    public int getSeconds();
}