package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.HashMap;

public class SkeletonTime implements ISkeleton{

    private HashMap<Integer, Time> mapTime = new HashMap<Integer, Time>();

    @Override
    public void process(DataInputStream dis, DataOutputStream dos) {
        // 1: cretion of new agenda
        // 2: write in agenda
        // 3: read agenda
        try {
            int codOp = dis.readInt();
            int identifier = 0;
            String key;
            int value = 0;
            Time time = null;
            switch (codOp) {
                case 1:
                    identifier = dis.readInt();
                    this.mapTime.put(identifier, new Time());
                    dos.writeBoolean(true);
                    break;
                case 2:
                    identifier = dis.readInt();
                    time = this.mapTime.get(identifier);
                    if (time != null) {
                        dos.writeBoolean(true);
                        dos.writeInt(time.getHour());
                    } else {
                        dos.writeBoolean(false);
                    }
                    break;
                case 3:
                    identifier = dis.readInt();
                    time = this.mapTime.get(identifier);
                    if (time != null) {
                        dos.writeBoolean(true);
                        dos.writeInt(time.getMinute());
                    } else {
                        dos.writeBoolean(false);
                    }
                    break;
                case 4:
                    identifier = dis.readInt();
                    time = this.mapTime.get(identifier);
                    if (time != null) {
                        dos.writeBoolean(true);
                        dos.writeInt(time.getSeconds());
                    } else {
                        dos.writeBoolean(false);
                    }
                    break;
                default:
                    dos.writeBoolean(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getIid() {
        return 2;
    }
}
