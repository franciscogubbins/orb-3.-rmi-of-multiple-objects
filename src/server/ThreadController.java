package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ThreadController implements  Runnable {

    Socket cs = null;
    DataInputStream dis = null;
    DataOutputStream dos = null;
    ISkeleton skeleton = null;

    public ThreadController(Socket cs, ISkeleton skeleton) throws IOException {
        this.cs = cs;
        this.dis = new DataInputStream(cs.getInputStream());
        this.dos = new DataOutputStream(cs.getOutputStream());
        this.skeleton = skeleton;
    }

    @Override
    public void run() {
        if (this.skeleton != null) {
            skeleton.process(this.dis, this.dos);
        }
        try {
            this.cs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
