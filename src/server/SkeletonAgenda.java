package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.HashMap;

public class SkeletonAgenda implements ISkeleton {

    private HashMap<Integer, Agenda> mapAgenda = new HashMap<Integer, Agenda>();
    private int iid = 1;

    public SkeletonAgenda() {
    }

    @Override
    public void process(DataInputStream dis, DataOutputStream dos) {
        // 1: cretion of new agenda
        // 2: write in agenda
        // 3: read agenda
        try {
            int codOp = dis.readInt();
            int identifier = 0;
            String key;
            int value = 0;
            Agenda agenda = null;
            switch (codOp) {
                case 1:
                    identifier = dis.readInt();
                    this.mapAgenda.put(identifier, new Agenda());
                    dos.writeBoolean(true);
                    break;
                case 2:
                    identifier = dis.readInt();
                    key = dis.readUTF();
                    value = dis.readInt();
                    agenda = this.mapAgenda.get(identifier);
                    if (agenda != null) {
                        agenda.write(key, value);
                        dos.writeBoolean(true);
                    } else {
                        dos.writeBoolean(false);
                    }
                    break;
                case 3:
                    identifier = dis.readInt();
                    key = dis.readUTF();
                    agenda = this.mapAgenda.get(identifier);
                    if (agenda != null) {
                        int resp = agenda.read(key);
                        dos.writeBoolean(true);
                        dos.writeInt(resp);
                    } else {
                        dos.writeBoolean(false);
                    }
                    break;
                default:
                    dos.writeBoolean(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getIid() {
        return 1;
    }
}
