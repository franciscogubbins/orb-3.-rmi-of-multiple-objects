package server;

interface IRepository {
    // Assigns a value d to a string key
    void write (String key, int d);
    // Returns the value of key
    int read (String key);
}
