package server;

import java.util.Hashtable;

public class Agenda implements IRepository{
    private Hashtable<String, Integer> ht = new Hashtable<String, Integer>();

    public void write(String s, int v) {
        ht.put(s, v);
    }
    public int read(String s) {
        return ht.get(s).intValue();
    }
}
