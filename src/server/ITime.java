package server;

public interface ITime {
    public int getHour();
    public int getMinute();
    public int getSeconds();
}